<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$aMenuLinks = Array(
	Array(
		"Летняя резиденция",
		"/residence/",
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Бассейны",
		"/pool/",
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Тарифы",
		"/rates/",
		Array(), 
		Array(), 
		"" 
	),	
	Array(
		"Услуги",
		"/services/",
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Vip",
		"/vip/",
		Array(),
		Array(),
		""
	),
    Array(
        "Галерея",
        "/gallery/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Детская зона",
        "/children-s-zone/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Новости",
        "/news/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Сотрудничество",
        "/cooperation/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Посетителям",
        "/visitors/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Контакты",
        "/contacts/",
        Array(),
        Array(),
        ""
    ),
);
?>