(function($) {

    $(function () {
        // Если нет поддержки тега <svg>
        if (!Modernizr.inlinesvg) {
            $('[data-png]').each(function(){
                var src = $(this).data('png');
                $(this).before($('<img src="' + src + '" alt="">')).remove();
            });
        }

        // Если нет поддержки картинки image.svg
        if (!Modernizr.svgasimg) {
            $('img[src $= ".svg"]').each(function(){
                var src = $(this).attr('src');
                $(this).attr('src', src.replace('.svg', '.png'));
            });
        }

    });
}(jQuery));


$('#btn_show_nav').on('click', function () {
    $(this).toggleClass('is_active');
    $('#nav').toggleClass('is_active');
    console.log($('#nav').hasClass('is_active'));
    if ($('#nav').hasClass('is_active')) {
        console.log('adg`ljdsdgsb');
        $('body').addClass('overflow-none')
    } else {
        $('body').removeClass('overflow-none')
    }
    
});

var top_change_color_header = false;
var has_chanche_color_header = $('#change_color_header').length ? true : false;
if (!has_chanche_color_header) {
    $('.header').addClass('dark');
} else {
    top_change_color_header = $('#change_color_header').offset().top;
}

$(window).scroll(function () {
    if (has_chanche_color_header) {
        $('.video_wrap .video').offset({ top: $(document).scrollTop()}); 
        if (top_change_color_header < $(document).scrollTop()) {
            $('.header').addClass('dark');
        } else if ($('.header').hasClass('dark')) {
            $('.header').removeClass('dark');
        }
    }
});

$('.hover').hover(function () {
    $('.video_wrap').offset({ top: $(document).scrollTop() });
    $('.video_wrap .video').addClass('is_hover');
}, function () {
    $('.video_wrap').offset({ top: 200 });
    $('.video_wrap .video').removeClass('is_hover');

});



$('.input_wrap .input').focus(function () {
    var wrap = $(this).parent('.input_wrap');
    $(wrap).addClass('focus');
});

$('.input_wrap .input').blur(function () {
    var wrap = $(this).parent('.input_wrap');
    if (!$(this).val().length) {
        $(wrap).removeClass('focus');
    }
});

function creat_super_placeholders() {
    var inputs = $('.input_wrap .input');
    if (inputs.length) {
        for (let index = 0; index < inputs.length; index++) {
            const element = inputs[index];
            var wrap = $(element).parent('.input_wrap');
            var text = $(element).attr('placeholder');
            var placeholder = '<div class="placeholder">' + text + '</div>';
            $(wrap).append(placeholder);
        }
    }
}

creat_super_placeholders();




if($('#img_slider').length) {
    $('#img_slider').slick({
        onAfterChange: function(){
            console.log('asd');  
            $('#img_slider_page').text($('#img_slider').slickCurrentSlide()+1);
        }
    });

    // On before slide change
    $('#img_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log(nextSlide);
        $('#img_slider_page').text(nextSlide < 9 ? '0' + (nextSlide+1) : nextSlide+1);
    });
}



$('.bg_waves').paroller({ factor: -0.3, type: 'foreground', direction: 'vertical' });

if ($(window).width() > 768) {
    $('.info_block__description').paroller({ factor: 0.1, type: 'foreground', direction: 'vertical' });
}



function initMap() {
    var center = {lat: 45.1291155, lng: 38.9846368};
    var map = new google.maps.Map(
        document.getElementById('map'), 
        {
            zoom: 12,
            center: center,
            styles: [
                { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [ { "color": "#444444" } ] },
                { "featureType": "administrative.locality", "elementType": "labels", "stylers": [ { "visibility": "on" } ] },
                { "featureType": "landscape", "elementType": "all", "stylers": [ { "color": "#f2f2f2" }, { "visibility": "simplified" } ] },
                { "featureType": "poi", "elementType": "all", "stylers": [ { "visibility": "on" } ] },
                { "featureType": "poi", "elementType": "geometry", "stylers": [ { "visibility": "simplified" }, { "saturation": "-65" }, { "lightness": "45" }, { "gamma": "1.78" } ] },
                { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "poi", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "road", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": 45 } ] },
                { "featureType": "road", "elementType": "labels", "stylers": [ { "visibility": "on" } ] },
                { "featureType": "road", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "road.highway", "elementType": "all", "stylers": [ { "visibility": "simplified" } ] },
                { "featureType": "road.highway", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "transit.line", "elementType": "geometry", "stylers": [ { "saturation": "-33" }, { "lightness": "22" }, { "gamma": "2.08" } ] },
                { "featureType": "transit.station.airport", "elementType": "geometry", "stylers": [ { "gamma": "2.08" }, { "hue": "#ffa200" } ] },
                { "featureType": "transit.station.airport", "elementType": "labels", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "transit.station.rail", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] },
                { "featureType": "transit.station.rail", "elementType": "labels.icon", "stylers": [ { "visibility": "simplified" }, { "saturation": "-55" }, { "lightness": "-2" }, { "gamma": "1.88" }, { "hue": "#ffab00" } ] },
                { "featureType": "water", "elementType": "all", "stylers": [ { "color": "#bbd9e5" }, { "visibility": "simplified" } ] }
            ]
        }
    );

    var marker = new google.maps.Marker({
        position: center,
        map: map,
        icon: '../bitrix/templates/main/images/pin.png'
    });

}

if ($('.main_block').length) {
    $('.header').addClass('white');
}  

var has_class_dark = false;

$('#btn_close_main_form').on('click', function () {
    $('#main_form').removeClass('is_visible');
    has_class_dark ? $('#header').addClass('dark') : '';
    $('body').removeClass('overflow-none')
});


$('#btn_write_to_us').on('click', function () {
    has_class_dark = $('#header').hasClass('dark');
    has_class_dark ? $('#header').removeClass('dark') : '';
    $('#main_form').toggleClass('is_visible');
    $('body').addClass('overflow-none')
});





// $('.img_wrap').paroller({
//     factor: 0.25,                // multiplier for scrolling speed and offset
//     // factorXs: 0.1,           // multiplier for scrolling speed and offset if window width is <576px
//     // factorSm: 0.2,           // multiplier for scrolling speed and offset if window width is <=768px
//     // factorMd: 0.2,           // multiplier for scrolling speed and offset if window width is <=1024px
//     // factorLg: 0.3,           // multiplier for scrolling speed and offset if window width is <=1200px
//     type: 'background',         // background, foreground
//     direction: 'vertical'       // vertical, horizontal
// });


if ($('#gallery_slider').length) {
    var images = $('#gallery_slider .glallery_slider__slide');
    var buttons = '';
    for (let index = 0; index < images.length; index++) {
        const element = images[index];
        buttons += '<button class="n_page">'+ (((index + 1) < 10 ? '0' : '') + (index + 1)) +'</button>'
        $('.pagination__pages').html(buttons);
    }

    $('#pagination__pages').slick({
        asNavFor: '#gallery_slider',
        verticalSwiping: true,
        vertical: true,
        slidesToShow: 4,
        slidesToScroll: -1,
        arrows: false,
        focusOnSelect: true,
        centerMode: true,
        verticalReverse: true,
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1150,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3
                }
            },
        ]
    });

    $('#gallery_slider').slick({
        arrows: true,
        slideToScroll: 1,
        asNavFor: '#pagination__pages'
    });

     $('#gallery_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log(nextSlide);
        $('#gallery_slider_page').text(nextSlide < 9 ? '0' + (nextSlide+1) : nextSlide+1);
        $('#now_page').text(nextSlide < 9 ? '0' + (nextSlide+1) : nextSlide+1);
    });
}






if ($('#main_header_slider').length) {


    
    $('#main_header_slider').on('init', function(){
        $('.main_header_slider__slide').each(function (index, element) {
            console.log(element);
            $(element).css('background-image', 'url(' + $(element).attr('data-img') + ')');
        });
    });

    $('#main_header_slider').slick({
        arrows: false,
        asNavFor: '#main_text_slider',
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    arrows: true
                }
            }
        ]
    });

    $('#main_header_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log(nextSlide);
        $('#main_header_slider_page').text(nextSlide < 9 ? '0' + (nextSlide+1) : nextSlide+1);
        $('#main_text_slider_page').text(nextSlide < 9 ? '0' + (nextSlide+1) : nextSlide+1);
        // $('.video_wrap .video.in_active').removeClass('in_active');
        $('.video_wrap .video.in_active').fadeTo( "slow", 0 );
        $($('.video_wrap .video')[nextSlide]).fadeTo( "slow", 1 );
        $($('.video_wrap .video')[nextSlide]).addClass('in_active');
        if (!$($('.video_wrap .video')[nextSlide]).prop('src')) {
            $($('.video_wrap .video')[nextSlide]).attr('src', $($('.video_wrap .video')[nextSlide]).attr('data-src'));
        }
    });


    $('#main_text_slider').slick({
        arrows: true,
        asNavFor: '#main_header_slider',
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    arrows: false
                }
            }
        ]
    });

}



if ($('#gallery_page_slider').length) {
    $('#btn_close_gallery').on('click', function () {
        $('#gallery').removeClass('is_visible');
    });

    $('#btn_gallery').on('click', function () {
        $('#gallery').addClass('is_visible');
    });

    $('#gallery_page_slider').slick({
        adaptiveHeight: true
    });

    $('#gallery').on('click', function (e) {
        var target = e ? e.target : event.srcElement;
        while ( target != this && target.nodeName.toLowerCase() != "img"  ) {
            target = target.parentNode;
        }
        if ( target == this ) { return; }
        var index = 0;
        while ( (target = target.previousSibling) ) {
            if ( target.nodeType === 1 ) {
                index++;
            }
        }
        $('#gallery_page_slider').slick('slickGoTo', index - 1);
        $('#gallery').removeClass('is_visible');
    });            
}



if ($('.main_block').length) {
    $('.main').addClass('p_vip');
}



function isVisible(tag) {
    var t = $(tag);
    var w = $(window);
    var wt = w.scrollTop();
    var tt = t.offset().top;
    var tb = tt + t.height();
    return ((tb <= wt + w.height()) && (tt >= wt));
}

$(window).scroll(function () {
    $('.services .img_wrap').each(function (index, element) {
        if (!$(element).prop("shown") && isVisible(element)) {
            $(element).prop("shown", true);
            $(element).addClass('is_visible');
        }
    });
});