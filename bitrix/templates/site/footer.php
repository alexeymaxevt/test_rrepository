<div class="bg_waves mobile_only"></div>
<? if ($APPLICATION->GetCurPage(false)=== '/'):?>
<div class="map_wrapper">
    <div class="map" id="map" style="width: 100%; height: 690px"></div>
</div>
<?endif; ?>
</main>

<div class="main_form" id="main_form">
    <button class="btn_close" id="btn_close_main_form" type="button">
        <div class="line"></div>
    </button>
    <?$APPLICATION->IncludeComponent("bitrix:main.feedback", "write_us", Array(
        "EMAIL_TO" => "evmax@stratosfera.digital",	// E-mail, на который будет отправлено письмо
        "EVENT_MESSAGE_ID" => array(	// Почтовые шаблоны для отправки письма
            0 => "7",
        ),
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",	// Сообщение, выводимое пользователю после отправки
        "REQUIRED_FIELDS" => array(	// Обязательные поля для заполнения
            0 => "NAME",
            1 => "EMAIL",
            2 => "MESSAGE",
        ),
        "USE_CAPTCHA" => "N",	// Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
    ),
        false
    );?>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-1">

            </div>
            <div class="col-md-2 d-flex flex-column justify-content-between">
                <div class="weather">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/site_weather.php"
                        ),
                        false
                    );?>
                </div>
            </div>

            <div class="col-sm-8 col-md-7 col-lg-6">
                <div class="contact">
                    <div class="contact__label">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_address_name.php"
                            ),
                            false
                        );?></div>
                    <div class="contact__value">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_address.php"
                            ),
                            false
                        );?>
                    </div>
                </div>
                <div class="contact">
                    <div class="contact__label">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_email_name.php"
                            ),
                            false
                        );?>
                    </div>
                    <div class="contact__value">
                        <span class="value_row">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => SITE_DIR."include/site_email_info.php"
                                ),
                                false
                            );?>
                        </span>
                        <span class="value_row">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => SITE_DIR."include/site_email_pr.php"
                                ),
                                false
                            );?>
                        </span>
                    </div>
                </div>
                <div class="contact">
                    <div class="contact__label">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_phone_name.php"
                            ),
                            false
                        );?>
                    </div>
                    <div class="contact__value">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_phone.php"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 col-lg-2 d-flex flex-column justify-content-between">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR."include/site_social.php"
                    ),
                    false
                );?>
            </div>

        </div>
        <div class="row">
            <div class="col-12 col-lg-1">

            </div>
            <div class="col-lg-10 d-flex justify-content-between">
                <div class="copyright">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/site_copyright.php"
                        ),
                        false
                    );?>
                </div>

                <div class="creater">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/site_create.php"
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.12.4.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-custom.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/slick.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.paroller.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/parallax.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/base.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/base-init.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/smooth-scroll.js"></script>

<script>
    SmoothScroll({ stepSize: 80, touchpadSupport: true });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7HLcrOIIhZVf6EkhjKE9JDd_liNSLc98&callback=initMap&language=ru" async defer></script>

</body>
</html>