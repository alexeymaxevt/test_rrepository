<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="ru">
<head>

    <?$APPLICATION->ShowHead();?>
<!--    <meta charset="utf-8">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="theme-color" content="#ccc">

<!--    <title>SANTORINI - Главная страница</title>-->
    <title><?$APPLICATION->ShowTitle()?></title>

    <link href="<?=SITE_TEMPLATE_PATH?>/styles/css/base.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/styles/css/tablet.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/styles/css/phablet.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/styles/css/phone.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.min.js"></script>
    <![endif]-->

</head>

<body>
<?$APPLICATION->ShowPanel()?>
<header class="header" id="header">
    <button class="btn_show_nav" id="btn_show_nav"><span class="line"></span></button>
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
);?>
    <div class="logo">
        <a class="link" href="/"></a>
    </div>
</header>

<main class="main">
    <button class="btn_write_to_us" id="btn_write_to_us"><span class="text">Напишите нам</span></button>
