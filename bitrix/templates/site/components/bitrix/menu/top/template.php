<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<nav class="nav" id="nav">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <?if (!empty($arResult)):?>
                    <div class="nav__list">

                        <?
                        foreach($arResult as $arItem):
                            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                                continue;
                            ?>
                            <?if($arItem["SELECTED"]):?>
                            <a href="<?=$arItem["LINK"]?>" class="link nav__link selected"><?=$arItem["TEXT"]?></a>
                        <?else:?>
                            <a href="<?=$arItem["LINK"]?>" class="link nav__link"><?=$arItem["TEXT"]?></a>
                        <?endif?>

                        <?endforeach?>

                    </div>
                <?endif?>

            </div>
        </div>
    </div>
</nav>
