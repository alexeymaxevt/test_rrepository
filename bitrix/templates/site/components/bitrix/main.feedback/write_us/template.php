<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<form class="container form">
    <?if(!empty($arResult["ERROR_MESSAGE"]))
    {
        foreach($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if(strlen($arResult["OK_MESSAGE"]) > 0)
    {
        ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
    }
    ?>
    <?=bitrix_sessid_post()?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-4">
            <div class="input_wrap"><input class="input" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="<?=empty($arResult["AUTHOR_NAME"]) ? GetMessage("MFT_NAME") : ''?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>></div>
        </div>
        <div class="col-sm-4">
            <div class="input_wrap"><input class="input" type="email" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" placeholder="<?=empty($arResult["AUTHOR_EMAIL"]) ? GetMessage("MFT_EMAIL") : ''?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="input_wrap">
                <textarea class="textarea input" placeholder="<?=GetMessage("MFT_MESSAGE")?>" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>></textarea>
            </div>
            <div class="checkbox">
                <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                    <div class="mf-captcha">
                        <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                        <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
                        <input type="text" name="captcha_word" size="30" maxlength="50" value="">
                    </div>
                <?endif;?>
                <input type="checkbox" name="" id="politic">
                <label class="label" for="politic">Я согласен с правилами обработки</label>
            </div>
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
            <input type="hidden" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
            <button class="btn_orange" name="submit"><?=GetMessage("MFT_SUBMIT")?></button>
        </div>
    </div>
</form>