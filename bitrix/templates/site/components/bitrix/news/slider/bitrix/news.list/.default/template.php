<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main_slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="main_header_slider_wraper">
                    <div class="main_header_slider_page" id="main_header_slider_page">01</div>
                    <div class="main_header_slider" id="main_header_slider">
                        <div class="main_header_slider__slide slide_header">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => SITE_DIR."include/site_slider_head_1.php"
                                ),
                                false
                            );?>
                        </div>
                        <div class="main_header_slider__slide slide_header">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => SITE_DIR."include/site_slider_head_2.php"
                                ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <div class="main_text_slider" id="main_text_slider">
                    <div class="main_text_slider__slide text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_slider_text_1.php"
                            ),
                            false
                        );?>
                    </div>
                    <div class="main_text_slider__slide text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/site_slider_text_2.php"
                            ),
                            false
                        );?>
                    </div>
                </div>
                <div class="slider_page" id="main_text_slider_page">
                    01
                </div>
            </div>
            <div class="col-sm-6">
                <div class="bg_waves slide_waves"></div>
            </div>
        </div>
    </div>
</div>