<div class="main_slider_video" id="main_slider_video">
    <div class="main_slider_video__slide">
        <div class="hover"></div>
        <div class="video_wrap">
            <video class="video in_active" src="/upload/video/Santorini.mp4" autoplay loop muted></video>
            <video class="video" data-src="/upload/video/Santorini.mp4" autoplay loop muted></video>
        </div>
    </div>
</div>