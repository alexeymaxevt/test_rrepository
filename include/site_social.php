<div class="socials">
    <a class="link link_trip" href="https://www.tripadvisor.ru/" target="_blank"><img src="/bitrix/templates/site/images/trip.png" data-bx-app-ex-src="#BXAPP0#" alt="" class="icon" data-bx-orig-src="/bitrix/templates/site/images/trip.png"></a>
    <a class="link link_fb" href="https://www.facebook.com/" target="_blank"><img src="/bitrix/templates/site/images/fb.png" data-bx-app-ex-src="#BXAPP1#" alt="" class="icon" data-bx-orig-src="/bitrix/templates/site/images/fb.png"></a>
    <a class="link link_inst" href="https://www.instagram.com/" target="_blank"><img src="/bitrix/templates/site/images/inst.png" data-bx-app-ex-src="#BXAPP2#" alt="" class="icon" data-bx-orig-src="/bitrix/templates/site/images/inst.png"></a>
    <a class="link link_vk" href="https://vk.com/" target="_blank"><img src="/bitrix/templates/site/images/vk.png" data-bx-app-ex-src="#BXAPP3#" alt="" class="icon" data-bx-orig-src="/bitrix/templates/site/images/vk.png"></a>
</div>